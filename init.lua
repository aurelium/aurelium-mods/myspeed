--[[
    MySpeed - Adds the ability to move fast.
    Copyright (C) 2018  za267

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-- minetest.register_chatcommand("antigravity", {
--     func = function(name, param)
--         local player = minetest.get_player_by_name(name)
--         if player.gravity = 1 then

--             player:set_physics_override({
--                 gravity = 0.1 -- set gravity to 10% of its original value
--                             -- (0.1 * 9.81)
--             })
--         else
--             player:set_physics_override({
--                 gravity = 1 -- set gravity to 10% of its original value
--                             -- (0.1 * 9.81)
--             })
--         end
--         return true, "you said " .. param .. "!"
--     end
-- })

-- register priv
minetest.register_privilege("xfast", {
        description = "Ultra high speed",
        give_to_singleplayer=false
})

minetest.register_chatcommand("fast", {
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        local playerspeed = player:get_physics_override()
        local newspeed = 1
        local newgrav = 1
        local newjump = 1
        local mesg = "player orig speed = " .. playerspeed["speed"] .. ";"

        if playerspeed["speed"] == 2 then
            newspeed = 1
            newgrav = 1
            newjump = 1
        else
            newspeed = 2
            newgrav = 1
            newjump = 2
        end
        player:set_physics_override({
            speed = newspeed,
            gravity = newgrav,
            jump = newjump
        })
        return true , mesg .. " speed is set to: " .. newspeed
    end
})



minetest.register_chatcommand("faster", {
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        local playerspeed = player:get_physics_override()
        local newspeed = 1
        local newgrav = 1
        local newjump = 1
        local mesg = "player orig speed = " .. playerspeed["speed"] .. ";"

		if minetest.check_player_privs(player, {xfast = true}) then
			if playerspeed["speed"] == 3 then
				newspeed = 1
				newgrav = 1
				newjump = 1
			else
				newspeed = 3
				newgrav = 1
				newjump = 2
			end
            player:set_physics_override({
                speed = newspeed, -- set speed 3 times the default. this should increase the available speed
                gravity = newgrav,
                jump = newjump
            })
            return true, mesg .. " speed is set to: " .. newspeed
        end
    end
})

minetest.register_chatcommand("fastest", {
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        local playerspeed = player:get_physics_override()
        local newspeed = 1
        local newgrav = 1
        local newjump = 1
        local mesg = "player orig speed = " .. playerspeed["speed"] .. ";"

		if minetest.check_player_privs(player, {xfast = true}) then
		    if playerspeed["speed"] == 6 then
		        newspeed = 1
		        newgrav = 1
		        newjump = 1
		    else
		        newspeed = 6
		        newgrav = 1
		        newjump = 2
		    end
		    player:set_physics_override({
		        speed = newspeed, -- set speed 3 times the default.
		        gravity = newgrav,
		        jump = newjump
		    })
		    return true, mesg .. " speed is set to: " .. newspeed
		end
    end
})

minetest.register_chatcommand("hyper", {
    func = function(name, param)
        local player = minetest.get_player_by_name(name)
        local playerspeed = player:get_physics_override()
        local newspeed = 1
        local newgrav = 1
        local newjump = 1
        local mesg = "player orig speed = " .. playerspeed["speed"] .. ";"

		if minetest.check_player_privs(player, {xfast = true}) then
		    if playerspeed["speed"] == 20 then
		        newspeed = 1
		        newgrav = 1
		        newjump = 1
		    else
		        newspeed = 20
		        newgrav = 0.6
		        newjump = 3
		    end
		    player:set_physics_override({
		        speed = newspeed, -- set speed 3 times the default.
		        gravity = newgrav,
		        jump = newjump
		    })
		    return true, mesg .. " speed is set to: " .. newspeed
		end
    end
})



